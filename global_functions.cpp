#ifndef _GLOBAL_FUNCS_
#define _GLOBAL_FUNCS_

#include "global_variables.h"

void wait_button(int button) {
    while(digitalRead(button) == OFF) {
        delay(50);
    }
}

bool is_pressed(int btn) {
    return digitalRead(btn) == OFF;
}

int title_x(const char *s) {
	int i = 0;
	while (*s) {
		s++;
		i++;
	}
	
	return (84 - i*6)/2;
}

#endif