#include <wiringPi.h>
#include <string.h>
#include <PCD8544.h>

#include "menu.h"


bool light = true;
bool is_done;

MenuItem setting3("Light: ON");

void changeLight() {
    light = !light;
    if (light) setting3.setTitle("Light: On");
    else setting3.setTitle("Light: OFF");
    menuBar.update();
    digitalWrite(BL, light);
}

void showLogo() {
    LCDclear();
    LCDshowLogo();
    LCDdisplay();

    while (1) {
        if (is_pressed(BTN1)) {
            wait_button(BTN1);

            break;
        }
    }

    menuBar.update();
}

void quit_menu() {
    is_done = true;
    is_quit = true;
}

void init_menu() {
    MenuItem *prog = new MenuItem("Program");
    MenuItem *sys  = new MenuItem("System");
    MenuItem *quit = new MenuItem("Quit");

    MenuItem *prog1 = new MenuItem("Detect Ball");
    MenuItem *prog2 = new MenuItem("Hello World");
    MenuItem *prog3 = new MenuItem("Calculator");
    MenuItem *prog4 = new MenuItem("Show Logo");

    prog->addItem(prog1);
    prog->addItem(prog2);
    prog->addItem(prog3);
    prog->addItem(prog4);

    MenuItem *setting1 = new MenuItem("LCD Contrast");
    MenuItem *setting2 = new MenuItem("LCD Bias");

    sys->addItem(setting1);
    sys->addItem(setting2);
    // sys->addItem(&setting3);

    menuBar.addItem(prog);
    menuBar.addItem(sys);
    menuBar.addItem(quit);

    
    prog4->setAction(showLogo);
    // setting3->setAction(changeLight);

    quit->setAction(quit_menu); 
    LCDclear();
    LCDdisplay();
}

void show_menu() {
    is_done     =   false;
    MenuBar root = menuBar;

    menuBar.update();

    while (!is_done) {

        if(is_pressed(BTN1)) {
            wait_button(BTN1);

            if (menuBar.currentMenu->getTitle() == root.currentMenu->getTitle()) {
                is_done = true;
            }
            
            menuBar.back();
        }
        if(is_pressed(BTN2)) {
            wait_button(BTN2);

            menuBar.up();
        }
        if(is_pressed(BTN3)) {
            wait_button(BTN3);

            menuBar.down();
        }

        if(is_pressed(BTN4)) {
            wait_button(BTN4);

            menuBar.enter();
        }
    }

}

