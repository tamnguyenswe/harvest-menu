//TODO: nghien cuu cai condition variable

#include    <stdio.h>
#include    <PCD8544.h>
#include    <wiringPi.h>
#include    <string>
#include    <thread>

#include    "menu/menu.cpp"
#include    "global_functions.cpp"

using namespace std;

// int title_x()

void initPin() {
    pinMode (BL, OUTPUT);
    digitalWrite(BL, HIGH);

    pinMode (BTN1, INPUT);
    pullUpDnControl (BTN1, PUD_UP) ;

    pinMode (BTN2, INPUT);
    pullUpDnControl (BTN2, PUD_UP) ;

    pinMode (BTN3, INPUT);
    pullUpDnControl (BTN3, PUD_UP) ;

    pinMode (BTN4, INPUT);
    pullUpDnControl (BTN4, PUD_UP) ;
}

void gif_run() {
    int i = 0;
    
        while (1) {
            if (is_continue) {
                if (i > 7) i = 0;
                LCDclear();
                LCDdrawbitmap(0, 0, gif[i], 84, 48, BLACK);
                LCDdisplay();
                i++;
            }
            delay(1200);
    
    }
}

int main() {
    wiringPiSetup();
    initPin();
    LCDInit();

    thread t1(gif_run);
    // show_menu();

    init_menu();

    // show_menu();
    while (!is_quit) {
        if (is_pressed(BTN1)) {
            wait_button(BTN1);
            is_continue = false;
            show_menu();

            is_continue = true;
            // gui_init();
        }

        if (is_pressed(BTN2)) {
            wait_button(BTN2);
            cout << title_x("MAIN MENU") << endl;
        }

        if (is_pressed(BTN4)) {
            break;
        }
    }

    LCDclear();
    LCDdisplay();

    return 0;
}